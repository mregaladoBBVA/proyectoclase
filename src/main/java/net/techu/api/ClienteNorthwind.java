package net.techu.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

// Se implementa Serializable para que se pueda pasar por la API
public class ClienteNorthwind implements Serializable {
    @JsonProperty("PersonID")
    private String id;
    @JsonProperty("Age")
    private Integer edad;
    @JsonProperty("Phone")
    private String telefono;
}
