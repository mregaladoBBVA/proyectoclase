package net.techu.api.controllers;

import net.techu.api.Producto;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductosController {
    private static Map<String, Producto> listaProductos = new HashMap<>();

    /* Inicializar lista de productos */
    static {
        Producto pr1 = new Producto();
        pr1.setId("PR1");
        pr1.setNombre("PRODUCTO 1");
        listaProductos.put(pr1.getId(), pr1);

        Producto pr2 = new Producto();
        pr2.setId("PR2");
        pr2.setNombre("PRODUCTO 2");
        listaProductos.put(pr2.getId(), pr2);
    }

    @GetMapping(value = "/productos")
    //@RequestMapping(value = "/productos", method = RequestMethod.GET)
    public Map<String, Producto> getProductos() {
        return listaProductos;
    }

    @Autowired
    ResourceLoader cargador;

    @GetMapping(value = "/v2/productos")
    public String getProductosDeFichero() {
        String productos = "";
        String nombreFichero = "classpath:static/producto.txt";
        Resource recursoFichero = cargador.getResource(nombreFichero);
        try {
            InputStream streamDatos = recursoFichero.getInputStream();
            byte[] bytesDocumento = streamDatos.readAllBytes();
            // Se lee el arreglo de bytes del archivo
            productos = new String(bytesDocumento, StandardCharsets.UTF_8);
            System.out.println(productos);
            streamDatos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
       return productos;
    }
    @GetMapping(value = "/v3/productos")
    public String getProductosDeOtraAPI() {
        String urlAPI = "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
        String productos = "";
        try {
            URL url = new URL(urlAPI);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("User-Agent", "MiApp");
            conexion.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            InputStreamReader entrada = new InputStreamReader(conexion.getInputStream());
            int respuesta = conexion.getResponseCode();
            System.out.println(new StringBuilder().append("Respuesta : ").append(respuesta));
            if (respuesta == HttpURLConnection.HTTP_OK) {
                BufferedReader lector = new BufferedReader(entrada);
                String lineaLeida;
                StringBuffer resultado = new StringBuffer();
                while ((lineaLeida = lector.readLine()) != null) {
                    resultado.append(lineaLeida);
                }
                lector.close();
                productos = resultado.toString();
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productos;
    }
    // Método Post para escritura de Productos en archivos con formato JSON o TXT
    @PostMapping(value = "/v3/productos/{formato}")
    public ResponseEntity<Object> addProductoFichero(@RequestBody Producto productoNuevo, @PathVariable("formato") String formato)
    {
        // Variables Iniciales
        String nombreFichero = "classpath:static/producto.txt";
        Resource recursoFichero = cargador.getResource(nombreFichero);
        File fichero = null;
        // Si el formato es JSON, se guardará el contenido en el archivo mediante un objeto JSON
       if (formato.toUpperCase().equals("JSON"))
       {
           JSONObject nuevo = new JSONObject();
           // Se genera el JSON con el método PUT
           nuevo.put("id",productoNuevo.getId());
           nuevo.put("nombre",productoNuevo.getNombre());
           try {
               fichero = recursoFichero.getFile();
               // el Objeto FileWriter debe tener el parámetro append en True para añadir productos al fichero
               OutputStreamWriter escritor = new FileWriter(fichero, true);
               escritor.append('\n' + nuevo.toString());
               escritor.close();
               return new ResponseEntity<>("Producto JSON añadido al fichero", HttpStatus.CREATED);
                } catch (IOException e) {
               e.printStackTrace();
           }
       }
        else{
            try {
                fichero = recursoFichero.getFile();
                // el Objeto FileWriter debe tener el parámetro append en True para añadir productos al fichero
                OutputStreamWriter escritor = new FileWriter(fichero, true);
                escritor.append(String.format("\nNuevo Producto: %s %s ", productoNuevo.getId(), productoNuevo.getNombre()));
                escritor.flush(); // Vuelco el contenido de memoria (pendiente) al disco y deja abierto el fichero
                escritor.close();  // Vuelco el contenido de memoria a disco y cierro el fichero
                return new ResponseEntity<>("Producto añadido al fichero", HttpStatus.CREATED);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("Error al añadir producto al fichero", HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/productos/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getProducto(@PathVariable("id") String id)
    {
        if (listaProductos.containsKey(id.toUpperCase()))
            return new ResponseEntity<>(listaProductos.get(id.toUpperCase()), HttpStatus.OK);
        return new ResponseEntity<>(String.format("No he encontrado el producto %s", id),
                                    HttpStatus.NOT_FOUND);
    }

    @PostMapping("/productos")
    public ResponseEntity<Object> addProducto(@RequestBody Producto productoNuevo)
    {
        if (listaProductos.containsKey(productoNuevo.getId().toUpperCase()))
            return new ResponseEntity<>("Producto ya existente", HttpStatus.CONFLICT);
        listaProductos.put(productoNuevo.getId(), productoNuevo);
        return new ResponseEntity<>(String.format("Producto %s dado de alta", productoNuevo.getId()),
                                    HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<Object> updateProducto(@PathVariable("id") String id,
                                                 @RequestBody Producto productoCambiado)
    {
        if (!id.toUpperCase().equals(productoCambiado.getId().toUpperCase()))
            return new ResponseEntity<>("Datos incorrectos", HttpStatus.CONFLICT);
        if (listaProductos.containsKey(id.toUpperCase())) {
            listaProductos.put(id, productoCambiado);
            return new ResponseEntity<>(String.format("Producto %s actualizado", id), HttpStatus.OK);
        }
        listaProductos.put(id, productoCambiado);
        return new ResponseEntity<>(String.format("Producto %s creado", id), HttpStatus.CREATED);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<Object> deleteProducto(@PathVariable("id") String id)
    {
        if (listaProductos.containsKey(id.toUpperCase())) {
            listaProductos.remove(id);
            return new ResponseEntity<>(String.format("Producto %s eliminado", id), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.format("Producto %s no encontrado", id), HttpStatus.NOT_FOUND);
    }


    //@RequestMapping("/productos")
    public String productos() {
        return "Lista de productos";
    }

    //@RequestMapping("/productos")
    public String productos(@RequestParam(value = "id", required = true) String id) {
        return String.format("Info del producto %s", id);
    }
}
