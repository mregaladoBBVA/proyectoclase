package net.techu.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ProductNortwind implements Serializable{
    @JsonProperty("ID")
    private String ID;
    @JsonProperty("Name")
    private String Name;
    @JsonProperty("Price")
    private String Price;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        this.Price = price;
    }
}
