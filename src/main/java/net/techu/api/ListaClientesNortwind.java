package net.techu.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaClientesNortwind implements Serializable {
    @JsonProperty("value")
    private List<ClienteNorthwind> clientes;
    public ListaClientesNortwind(){
        clientes = new ArrayList<>();
    }
    public List<ClienteNorthwind> getClientes(){
        return clientes;
    }
}