package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

// Se  implementa la Clase Vehiculo de forma serializable para transformar la clase y poder enviarla como TXT
// Se indica la Collection en donde se almacenará la clase (para MongoDB)
@Document(collection = "vehiculos")
//Se indica el orden de las propiedades del JSON a guardar
@JsonPropertyOrder({"id","marca","modelo"})
public class Vehiculo implements Serializable {
//  Se identifica el campo que será Id y si se desea se indica que no se permiten valores Nulos
    @Id
    @NotNull
    private String id;
    @NotNull
    private String marca;
    private String modelo;
    public Vehiculo(){}

    public Vehiculo(String id, String marca, String modelo){
        this.setId(id);
        this.setMarca(marca);
        this.setModelo(modelo);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
