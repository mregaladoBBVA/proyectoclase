package net.techu.api.repository;

import net.techu.api.models.Vehiculo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
// Para indicar a SpringBoot que la clase es un Repository
@Repository
public class VehiculoRepositoryImp implements VehiculoRepository{
// Propiedades de la Clase
    private final MongoOperations mongoOperations;
//  Para indicar que sea el primer método en ser ejecutado
    @Autowired
    public VehiculoRepositoryImp(MongoOperations mongoOperations){
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Vehiculo> findAll() {
        // Se obtiene una lista con todos los vehículos de la colección
        List<Vehiculo> vehiculos = this.mongoOperations.find(new Query(), Vehiculo.class);
        return vehiculos;
    }

    @Override
    public Vehiculo findOne(String id) {
        Vehiculo encontrado = this.mongoOperations.findOne(new Query(Criteria.where("id").is(id)), Vehiculo.class);
        return encontrado;
    }

    @Override
    public Vehiculo saveVehiculo(Vehiculo veh) {
        this.mongoOperations.save(veh);
        return this.findOne(veh.getId());
    }

    @Override
    public void updateVehiculo(Vehiculo veh) {
        this.mongoOperations.save(veh);
    }

    @Override
    public void deleteVehiculo(String id) {
       this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)),Vehiculo.class);
    }
}
