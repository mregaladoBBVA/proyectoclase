package net.techu.api.repository;

import net.techu.api.models.Vehiculo;

import java.util.List;

public interface VehiculoRepository {
  //Se define un método para listar todos los elementos y otro para una búsqueda individual
  List<Vehiculo> findAll();
  Vehiculo findOne(String id);

  //Se definen métodos de actualización de Vehículos para añadir, actualizar y eliminar
  public Vehiculo saveVehiculo(Vehiculo veh);
  public void updateVehiculo(Vehiculo veh);
  public void deleteVehiculo(String id);
}
